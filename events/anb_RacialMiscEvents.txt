namespace = racial_misc

# Half-orc heir
country_event = {
	id = racial_misc.1
	title = racial_misc.1.t
	desc = racial_misc.1.d
	picture = NEW_HEIR_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		OR = {
			AND = {
				ruler_is_human = yes
				consort_is_orc = yes
			}
			AND = {
				ruler_is_orc = yes
				consort_is_human = yes
			}
			AND = {
				ruler_is_human = yes
				consort_is_half_orc = yes
			}
			AND = {
				ruler_is_half_orc = yes
				consort_is_human = yes
			}
			AND = {
				ruler_is_orc = yes
				consort_is_half_orc = yes
			}
			AND = {
				ruler_is_half_orc = yes
				consort_is_orc = yes
			}
		}
		has_heir = yes
		heir_is_half_orc = no
	}
	
	option = {		# Good
		name = racial_misc.1.a
		set_heir_culture = half_orc
	}
}