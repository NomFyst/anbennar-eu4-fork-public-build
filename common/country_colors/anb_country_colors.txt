# Country unit colors.

# example_tag_template = {
    # color1= { 205 32 26  } #60% of character
    # color2= { 255 255 255  } #30% of character
    # color3= { 70 50 40  } #Up to 10% -> try map color here
# }

#colors
#lorentish red	{ 237 28 36 }
#lorentish lilac	{ 168 128 186 }
#lorentish green	{ 75 131 61 }
#lorentish white	{ 209 211 212 }


#Lorent
A01 = {
    color1= { 237 28 36 }
    color2= { 75 131 61 }
    color3= { 168 128 186 }
}

#Deranne
A02 = {
    color1= { 43 57 144 }
    color2= { 75 131 61 }
    color3= { 218 28 92 }
}

#Madelaire
A14 = {
    color1= { 209 211 212 }
    color2= { 168 128 186 }
    color3= { 237 28 36 }
}

#Eilisin
A15 = {
    color1= { 168 128 186 }
    color2= { 237 28 36 }
    color3= { 209 211 212 }
}

#Rubenaire
A18 = {
    color1= { 164 29 33 }
    color2= { 75 131 61 }
    color3= { 209 211 212 }
}

#Appleton
A64 = {
    color1= { 237 28 36 }
    color2= { 164 255 77 }
    color3= { 237 28 36 }
}

#Wesdam
A04 = {
    color1= { 28 124 140 }
    color2= { 237 28 36 }
    color3= { 56 56 56 }
}

#Tellum
A86 = {
    color1= { 215 119 176 }
    color2= { 254 231 166 }
    color3= { 215 119 176 }
}

#Beepeck
A12 = {
    color1= { 255 222 23 }
    color2= { 65 64 66 }
    color3= { 0 166 81 }
}

#Corintar
B02 = {
    color1= { 182 53 53 }
    color2= { 242 242 242 }
    color3= { 229 103 90 }
}

#Orda Aldresia
A77 = {
    color1= { 36 102 177 }
    color2= { 241 242 242 }
    color3= { 255 223 132 }
}

#Sword Covenant
B19 = {
    color1= { 65 64 66 }
    color2= { 241 242 242 } 
    color3= { 130 208 214 }
}

#Bisan
A34 = {
    color1= { 0 148 68 }
    color2= { 0 148 68 }
    color3= { 233 193 30 }
}

#Portnamm
A19 = {
    color1= { 193 255 120 }
    color2= { 255 204 28 }
    color3= { 59 54 149 }
}

#Cobalt Company
B03 = {
    color1= { 32 63 123 }
    color2= { 29 30 30 }
    color3= { 91 80 61 }
}

#Alenor
B40 = {
    color1= { 32 63 123 }
    color2= { 29 30 30 }
    color3= { 91 80 61 }
}

#Estallen
A93 = {
    color1= { 247 179 209 }
    color2= { 159 207 111 }
    color3= { 188 77 138 }
}

#Themarenn
A95 = {
    color1= { 244 246 247 }
    color2= { 103 205 237 }
    color3= { 189 32 60 }
}

